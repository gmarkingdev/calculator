def calculate(first_operand, op, second_operand):
    value = 0
    if op == "+":
        value = int(first_operand) + int(second_operand)
    elif op == "-":
        value = int(first_operand) - int(second_operand)
    elif op == "*":
        value = int(first_operand) * int(second_operand)
    elif op == "/" and int(second_operand) != 0:
        value= int(first_operand) / int(second_operand)
    return value

def compute_expression(expression):
    first_operand = expression[0]
    i = 1
    while i < len(expression):
        op = expression[i]
        second_operand = expression[i + 1]
        first_operand = Calculator.calculate(first_operand, op, second_operand)
        i += 2
    return first_operand