def get_input():
    expression = input()
    if expression == "exit":
        exit()
    return "".join(expression.rstrip())