import calculator
import input_provider

def main():
    expression = import.get_input()
    while(True):
        arguments = expression.split(" ")
        value = calculator.compute_expression(arguments)
        print(value)
        expression = input_provider.get_input()

if __name__ == '__main__':
    main()